from django.contrib.auth.models import User
from rest_framework import serializers

from ..models import Room, Message


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ('name', 'label')


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('formatted_timestamp', 'handle', 'message')