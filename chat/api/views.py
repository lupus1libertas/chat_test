from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny, IsAuthenticated
from haikunator import Haikunator

from .serializers import UserSerializer, RoomSerializer, MessageSerializer
from ..models import Room, Message

# --- User views

class RegisterView(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    model = User
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data['username']
        password = request.data['password']
        try:
            user = User.objects.get(username=username)
            return Response({'success': False,
                            'message': 'User with such username already exists'})
        
        except User.DoesNotExist:
            user = User.objects.create(username=username)
            user.set_password(password)
            user.save()
            token = Token.objects.create(user=user)
            token.save()
            return Response({'success': True,
                             'token': token.key},
                            status=status.HTTP_201_CREATED)


class LoginView(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    model = User
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data['username']
        password = request.data['password']
        user = authenticate(username=username, password=password)
        if user:
            token, created = Token.objects.get_or_create(user=user)
            return Response({'success': True,
                            'token': token.key},
                            status=status.HTTP_200_OK)
        
        return Response({'success': False,
                        'message': 'Wrong username or password'},
                        status=status.HTTP_401_UNAUTHORIZED)

# --- end User views

# --- Chat views

class RoomCreateView(generics.CreateAPIView):
    model = Room
    serializer_class = RoomSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        haikunator = Haikunator()
        new_room = None
        while not new_room:
            label = haikunator.haikunate()
            if Room.objects.filter(label=label).exists():
                continue
            new_room = Room.objects.create(label=label)

        serializer = self.get_serializer(new_room)
        headers = self.get_success_headers(serializer.data)
        
        return Response(serializer.data, 
                        status=status.HTTP_201_CREATED, 
                        headers=headers)


class RoomsListView(generics.ListAPIView):
     serializer_class = RoomSerializer
     queryset = Room.objects.all()
     permission_classes = (IsAuthenticated,)
     model = Room


class MessagesListView(generics.ListAPIView):
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated,)
    model = Message

    def get_queryset(self):
        label = self.kwargs.get('label', None)
        try:
            room = Room.objects.get(label=label)
        except Room.DoesNotExist:
            return []
        return room.messages.all()

# --- end Chat views
