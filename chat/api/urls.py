from django.conf.urls import url, include
# from .views import RegisterView, LoginView
import views


urlpatterns = [
    url(r'^register/$', views.RegisterView.as_view()),
    url(r'^login/$', views.LoginView.as_view()),
    url(r'^rooms/$', views.RoomsListView.as_view()),
    url(r'^room/$', views.RoomCreateView.as_view()),
    url(r'^room/(?P<label>[\w-]{,50})/messages/', views.MessagesListView.as_view()),
]