from django.conf.urls import include, url
from . import views

app_name = 'chat'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^api/', include('chat.api.urls')),
]