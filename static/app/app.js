(function () {
  'use strict';

  angular.module('app', ['ui.router', 'ui.bootstrap', 'ngStorage']);
})();
(function () {
    'use strict';

    angular.module('app').constant('appConfig', new AppConfig());

    function AppConfig() {
        this.api_root_url = window.location.origin;
        this.serverDomain = window.location.host;

        // When we're using HTTPS, use WSS too.
        this.ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
        this.socketServerUrl = this.ws_scheme + '://' + this.serverDomain;
        // this.api_static = this.api_root_url + '/static/';
    }
})();
(function () {
    'use strict';

    angular.module('app').config(routesSetter);

    function routesSetter($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/rooms/");
        $stateProvider.state('rooms', {
            url: "/rooms/",
            templateUrl: "/static/app/chat/templates/rooms-list.html",
            controller: "RoomController as room"
        }).state('chat-room', {
            url: "/chat-room/:roomLabel",
            params: {
                roomLabel: undefined
            },
            templateUrl: "/static/app/chat/templates/chat-room.html",
            controller: "ChatController as chat"
        });
    }
})();
(function () {
    'use strict';

    angular.module('app').factory('chatService', chatService);
    chatService.$inject = ['appConfig', '$http', '$rootScope', 'userService'];

    function chatService(appConfig, $http, $rootScope, userService) {
        var self = this;
        self._roomLabel = '';

        self.getAllMessages = getAllMessages;
        self.sendMessage = sendMessage;
        self.enterRoom = enterRoom;
        self.leaveRoom = leaveRoom;

        return self;

        //---

        // --- Public

        function enterRoom(roomLabel) {
            if (self._chatsock) {
                leaveRoom();
            }

            self._roomLabel = roomLabel;
            var socketRoomUrl = appConfig.socketServerUrl + '/chat/' + self._roomLabel;
            self._chatsock = new ReconnectingWebSocket(socketRoomUrl);
            self._chatsock.addEventListener('message', onMessage);
        }

        function leaveRoom() {
            self._roomLabel = '';
            self._chatsock.removeEventListener('message', onMessage);
            self._chatsock.close();
        }

        function sendMessage(messageText) {
            var message = {
                handle: userService.getUserName(),
                message: messageText
            };
            self._chatsock.send(JSON.stringify(message));
        }

        function getAllMessages() {
            if (!self._roomLabel) {
                throw new Error('Room label not defined');
            }
            return $http({
                method: 'GET',
                headers: {
                    'Authorization': 'Token ' + userService.getToken()
                },
                url: appConfig.api_root_url + '/api/room/' + self._roomLabel + '/messages/'
            });
        }

        // --- end Public

        // --- Handlers

        function onMessage(message) {
            var data = JSON.parse(message.data);
            $rootScope.$emit('chatService:MessageReceived', data);
        }

        // --- end Handlers
    }
})();
(function () {
    'use strict';

    angular.module('app').factory('loggerService', loggerService);
    loggerService.$inject = [];

    function loggerService() {
        var self = this;
        self.log = log;

        return self;

        //---
        function log(message) {
            console.log(message);
        }
    }
})();
(function () {
    'use strict';

    angular.module('app').factory('roomService', roomService);
    roomService.$inject = ['$q', '$http', 'appConfig', 'userService'];

    function roomService($q, $http, appConfig, userService) {
        var self = this;
        self.getAvailableRooms = getAvailableRooms;
        self.createRoom = createRoom;

        return self;

        //---
        function createRoom() {
            return $http({
                method: 'POST',
                headers: {
                    'Authorization': 'Token ' + userService.getToken()
                },
                url: appConfig.api_root_url + '/api/room/'
            });
        }
        function getAvailableRooms() {
            return $http({
                method: 'GET',
                headers: {
                    'Authorization': 'Token ' + userService.getToken()
                },
                url: appConfig.api_root_url + '/api/rooms/'
            });
        }
    }
})();
(function () {
    'use strict';

    angular.module('app').factory('userService', userService);
    userService.$inject = ['appConfig', '$http', '$rootScope', 'loggerService', '$sessionStorage'];

    function userService(appConfig, $http, $rootScope, loggerService, $sessionStorage) {
        var self = this;
        initData();

        self.getUserName = getUserName;
        self.getToken = getToken;
        self.userLogined = userLogined;
        self.login = login;
        self.logout = logout;
        self.register = register;

        return self;

        //---

        // --- Public

        function getToken() {
            return self._token;
        }

        function register(username, password) {
            return $http({
                url: appConfig.api_root_url + '/api/register/',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                data: {
                    username: username,
                    password: password
                }
            }).then(onUserManageSuccess).catch(function (errorText) {
                loggerService.log('Error in userService:register Error text: ' + errorText);
                throw new Error(errorText);
            });
        }

        function login(username, password) {
            return $http({
                url: appConfig.api_root_url + '/api/login/',
                method: "POST",
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                data: {
                    username: username,
                    password: password
                }
            }).then(onUserManageSuccess).catch(function (errorData) {
                loggerService.log('Error in userService:login Error text: ' + errorData.data.message);
                throw new Error(errorData.data.message);
            });
        }

        function logout() {
            clearSessionStorage();
            initData();
        }

        function getUserName() {
            return self._username;
        }

        function userLogined() {
            return self._userLoginedFlag;
        }

        // --- end Public


        // --- Utils

        function clearSessionStorage() {
            setSessionStorage(undefined, undefined);
        }

        function setSessionStorage(username, token) {
            $sessionStorage.username = username;
            $sessionStorage.token = token;
        }

        function initData() {
            self._username = $sessionStorage.username || undefined;
            self._userLoginedFlag = $sessionStorage.token ? true : false;
            self._token = $sessionStorage.token || undefined;
        }

        // --- end Utils

        // --- Handlers

        function onUserManageSuccess(response) {
            if (!response.data.success) {
                throw new Error(response.data.message);
            }
            setSessionStorage(response.config.data.username, response.data.token);
            initData();

            return response;
        }

        // --- end Handlers
    }
})();
(function () {
    'use strict';

    angular.module('app').controller('ChatController', ChatController);
    ChatController.$inject = ['chatService', 'loggerService', '$rootScope', '$scope', '$stateParams', '$window', 'userService'];

    function ChatController(chatService, loggerService, $rootScope, $scope, $stateParams, $window, userService) {
        var self = this;
        initData();
        var roomLabel = $stateParams.roomLabel;

        self.sendMessage = sendMessage;
        self.onKeyDown = onKeyDown;

        chatService.enterRoom(roomLabel);
        $rootScope.$on('chatService:MessageReceived', onMessageReceived);

        $rootScope.$on('UserManagementController:UserLoggedOut', onUserLoggedOut);
        $rootScope.$on('UserManagementController:UserLogined', onUserLogined); // Yes, same handlers
        $rootScope.$on('UserManagementController:UserRegistered', onUserLogined); // Yes, same handlers

        getAllMessages();

        //---

        // --- Public

        function onKeyDown(event) {
            var enterKey = 13;
            if (event.keyCode === enterKey) {
                sendMessage(self.messageText);
            }
        }

        function sendMessage(messageText) {
            chatService.sendMessage(messageText);
            self.messageText = '';
        }

        // --- end Public

        // --- Utils

        function initData() {
            self.messageText = '';
            self.messages = [];
            self.userLogined = userService.userLogined();
            self.link = $window.location.href;
        }

        function getAllMessages() {
            if (!userService.userLogined()) {
                return;
            }

            chatService.getAllMessages().then(function (response) {
                self.messages = response.data;
            }).catch(function (errorText) {
                loggerService.log('Error in ChatController:getAllMessages Error text: ' + errorText);
            });
        }

        // --- end Utils

        // --- handlers

        function onMessageReceived(event, data) {
            self.messages.push(data);
            $scope.$digest();
        }

        function onUserLoggedOut(event, data) {
            initData();
        }

        function onUserLogined() {
            initData();
            getAllMessages();
        }

        // --- end Handlers
    }
})();
(function () {
    'use strict';

    angular.module('app').controller('RoomController', RoomController);
    RoomController.$inject = ['roomService', 'loggerService', 'userService', '$rootScope', '$state'];

    function RoomController(roomService, loggerService, userService, $rootScope, $state) {
        var self = this;
        initData();

        self.createRoom = createRoom;

        $rootScope.$on('UserManagementController:UserLogined', onUserManageChange);
        $rootScope.$on('UserManagementController:UserRegistered', onUserManageChange);
        $rootScope.$on('UserManagementController:UserLoggedOut', onUserManageChange);

        getAvailableRooms();

        //---

        // --- Public

        function createRoom() {
            roomService.createRoom().then(function (response) {
                $state.transitionTo('chat-room', { roomLabel: response.data.label });
            }).catch(function (errorText) {
                alert(errorText);
            });
        }

        // --- end Public

        // --- Handlers

        function onUserManageChange(event, data) {
            initData();
            getAvailableRooms();
        }

        // --- end Handlers

        // --- Utils

        function initData() {
            self.rooms = [];
            self.userLogined = userService.userLogined();
        }

        function getAvailableRooms() {
            if (!userService.userLogined()) {
                return;
            }
            roomService.getAvailableRooms().then(function (response) {
                self.rooms = response.data;
            }).catch(function (errorText) {
                loggerService.log('Error in RoomController:getAvailableRooms. Error text: ' + errorText);
            });
        }

        // --- end Utils
    }
})();
(function () {
    'use strict';

    angular.module('app').controller('LoginFormController', LoginFormController);
    LoginFormController.$inject = ['$uibModalInstance'];

    function LoginFormController($uibModalInstance) {
        var self = this;

        self.login = '';
        self.password = '';

        self.submit = submit;
        self.cancel = cancel;

        //---
        function submit() {
            var result = {
                login: self.login,
                password: self.password
            };
            $uibModalInstance.close(result);
        }

        function cancel() {
            var reason = {
                msg: 'click on cancel'
            };
            $uibModalInstance.dismiss(reason);
        }
    }
})();
(function () {
    'use strict';

    angular.module('app').controller('UserManagementController', UserManagementController);
    UserManagementController.$inject = ['userService', '$uibModal', 'loggerService', '$rootScope'];

    function UserManagementController(userService, $uibModal, loggerService, $rootScope) {
        var self = this;
        initData();

        self.login = login;
        self.logout = logout;
        self.register = register;

        var formParams = {
            animation: true,
            controller: 'LoginFormController as form',
            size: 'md'
        };

        //---

        // --- Public

        function login() {
            var formParamsClone = Object.assign({
                templateUrl: "/static/app/user-management/templates/login-form.html"
            }, formParams);

            var loginInstance = $uibModal.open(formParamsClone);

            loginInstance.result.then(function (result) {
                userService.login(result.login, result.password).then(loginSuccess).catch(function (errorText) {
                    alert(errorText);
                });
            });
        }

        function logout() {
            userService.logout();
            self.userLogined = userService.userLogined();
            self.userName = userService.getUserName();
            $rootScope.$emit('UserManagementController:UserLoggedOut');
        }

        function register() {
            var formParamsClone = Object.assign({
                templateUrl: "/static/app/user-management/templates/register-form.html"
            }, formParams);

            var registerInstance = $uibModal.open(formParamsClone);

            registerInstance.result.then(function (result) {
                userService.register(result.login, result.password).then(registerSuceess).catch(function (errorText) {
                    alert(errorText);
                });
            });
        }

        // --- end Public

        // --- Utils

        function initData() {
            self.userLogined = userService.userLogined();
            self.userName = userService.getUserName();
        }

        // --- end Utils

        // --- Handlers

        function loginSuccess(response) {
            initData();
            $rootScope.$emit('UserManagementController:UserLogined');
        }

        function registerSuceess() {
            initData();
            $rootScope.$emit('UserManagementController:UserRegistered');
        }

        // --- end Handlers
    }
})();