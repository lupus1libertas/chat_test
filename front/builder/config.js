'use strict';

const config = {};

config.srcDir = '../src';
config.srcDirJs = `${config.srcDir}/app`;
config.staticDir = '../../static';

config.paths = {
  in: {
    js: [
      `${config.srcDirJs}/**/*.module.js`,
      `${config.srcDirJs}/**/*.config.js`,
      `${config.srcDirJs}/**/*.routes.js`,
      `${config.srcDirJs}/**/*.service.js`,
      `${config.srcDirJs}/**/*.controller.js`,
      `${config.srcDirJs}/**/*.directive.js`,
    ],
    jsLibs: [
      `${config.srcDirJs}/libs/**/*.js`,
    ],
    html: [
      `${config.srcDir}/**/*.html`
    ]
  },
  out: {
    jsLibs: `${config.staticDir}/app/libs`,
    js: `${config.staticDir}/app`,
    html: `${config.staticDir}/`
  },
};

config.babel = {
  presets: ['es2015'],
  plugins: ['transform-remove-strict-mode'],
};

module.exports = config;