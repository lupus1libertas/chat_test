(() => {
    'use strict';

    angular.module('app').controller('RoomController', RoomController);
    RoomController.$inject = ['roomService', 'loggerService', 'userService', '$rootScope', '$state'];

    function RoomController(roomService, loggerService, userService, $rootScope, $state) {
        let self = this;
        initData();

        self.createRoom = createRoom;

        $rootScope.$on('UserManagementController:UserLogined', onUserManageChange);
        $rootScope.$on('UserManagementController:UserRegistered', onUserManageChange);
        $rootScope.$on('UserManagementController:UserLoggedOut', onUserManageChange);
        
        getAvailableRooms();
        
        //---
        
        // --- Public
        
        function createRoom() {
            roomService.createRoom()
            .then((response) => {
                $state.transitionTo('chat-room', {roomLabel: response.data.label});
            })
            .catch((errorText) => {
                alert(errorText);
            });
        }

        // --- end Public

        // --- Handlers

        function onUserManageChange(event, data) {
            initData();
            getAvailableRooms();
        }

        // --- end Handlers

        // --- Utils

        function initData() {
            self.rooms = [];
            self.userLogined = userService.userLogined();
        }

        function getAvailableRooms() {
            if (!userService.userLogined()){
                return;
            }
            roomService.getAvailableRooms()
            .then((response) => {
                self.rooms = response.data;
            })
            .catch((errorText) => {
                loggerService.log(`Error in RoomController:getAvailableRooms. Error text: ${errorText}`);
            });
        }

        // --- end Utils
    }
})();