(() => {
    'use strict';

    angular.module('app').controller('ChatController', ChatController);
    ChatController.$inject = ['chatService', 'loggerService', '$rootScope', '$scope', '$stateParams', '$window', 'userService'];

    function ChatController(chatService, loggerService, $rootScope, $scope, $stateParams, $window, userService) {
        let self = this;
        initData();
        const roomLabel = $stateParams.roomLabel;

        self.sendMessage = sendMessage;
        self.onKeyDown = onKeyDown;

        chatService.enterRoom(roomLabel);
        $rootScope.$on('chatService:MessageReceived', onMessageReceived);
        
        $rootScope.$on('UserManagementController:UserLoggedOut', onUserLoggedOut);
        $rootScope.$on('UserManagementController:UserLogined', onUserLogined); // Yes, same handlers
        $rootScope.$on('UserManagementController:UserRegistered', onUserLogined); // Yes, same handlers
        
        getAllMessages();
        
        //---

        // --- Public

        function onKeyDown(event) {
            const enterKey = 13;
            if (event.keyCode === enterKey) {
                sendMessage(self.messageText);
            }
        }
        
        function sendMessage(messageText) {
            chatService.sendMessage(messageText);
            self.messageText = '';
        }
        
        // --- end Public

        // --- Utils
        
        function initData() {
            self.messageText = '';
            self.messages = [];
            self.userLogined = userService.userLogined();
            self.link = $window.location.href;
        }

        function getAllMessages() {
            if (!userService.userLogined()) {
                return;
            }

            chatService.getAllMessages()
            .then((response) => {
                self.messages = response.data;
            })
            .catch((errorText) => {
                loggerService.log(`Error in ChatController:getAllMessages Error text: ${errorText}`);
            });
        }

        // --- end Utils

        // --- handlers
        
        function onMessageReceived(event, data) {
            self.messages.push(data);
            $scope.$digest();
        }

        function onUserLoggedOut(event, data) {
            initData();
        }

        function onUserLogined() {
            initData();
            getAllMessages();
        }
        
        // --- end Handlers
    }
})();