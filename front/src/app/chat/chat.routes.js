(() => {
    'use strict';

    angular.module('app').config(routesSetter);
    
    function routesSetter($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/rooms/");
        $stateProvider
            .state('rooms', {
                url: "/rooms/",
                templateUrl: "/static/app/chat/templates/rooms-list.html",
                controller: "RoomController as room",
            })
            .state('chat-room', {
                url: "/chat-room/:roomLabel",
                params: {
                    roomLabel: undefined
                },
                templateUrl: "/static/app/chat/templates/chat-room.html",
                controller: "ChatController as chat",
            })
            
    }
})();