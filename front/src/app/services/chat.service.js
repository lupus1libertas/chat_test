(() => {
    'use strict';
    
    angular.module('app').factory('chatService', chatService);
    chatService.$inject = ['appConfig', '$http', '$rootScope', 'userService'];

    function chatService(appConfig, $http, $rootScope, userService) {
        let self = this;
        self._roomLabel = '';
        
        self.getAllMessages = getAllMessages;
        self.sendMessage = sendMessage;
        self.enterRoom = enterRoom;
        self.leaveRoom = leaveRoom;

        return self;

        //---

        // --- Public

        function enterRoom(roomLabel) {
            if (self._chatsock) {
                leaveRoom();
            }

            self._roomLabel = roomLabel;
            const socketRoomUrl = `${appConfig.socketServerUrl}/chat/${self._roomLabel}`;
            self._chatsock = new ReconnectingWebSocket(socketRoomUrl);
            self._chatsock.addEventListener('message', onMessage) ;
        }

        function leaveRoom() {
            self._roomLabel = '';
            self._chatsock.removeEventListener('message', onMessage);
            self._chatsock.close();
        }

        function sendMessage(messageText) {
            const message = {
                handle: userService.getUserName(),
                message: messageText,
            }
            self._chatsock.send(JSON.stringify(message));
        }

        function getAllMessages() {
            if (!self._roomLabel) {
                throw new Error('Room label not defined');
            }
            return $http({
                method: 'GET',
                headers: {
                    'Authorization': `Token ${userService.getToken()}`,
                },
                url: `${appConfig.api_root_url}/api/room/${self._roomLabel}/messages/`
            });
        }

        // --- end Public

        // --- Handlers

        function onMessage(message) {
            var data = JSON.parse(message.data);
            $rootScope.$emit('chatService:MessageReceived', data)
        }

        // --- end Handlers
    }
})();