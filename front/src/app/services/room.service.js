(() => {
    'use strict';
    
    angular.module('app').factory('roomService', roomService);
    roomService.$inject = ['$q', '$http', 'appConfig', 'userService'];

    function roomService($q, $http, appConfig, userService) {
        let self = this;
        self.getAvailableRooms = getAvailableRooms;
        self.createRoom = createRoom;

        return self;

        //---
        function createRoom() {
            return $http({
                method: 'POST',
                headers: {
                    'Authorization': `Token ${userService.getToken()}`,
                },
                url: `${appConfig.api_root_url}/api/room/`
            });
        }
        function getAvailableRooms() {
            return $http({
                method: 'GET',
                headers: {
                    'Authorization': `Token ${userService.getToken()}`,
                },
                url: `${appConfig.api_root_url}/api/rooms/`
            });
        }
    }
})();