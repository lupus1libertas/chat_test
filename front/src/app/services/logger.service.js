(() => {
    'use strict';
    
    angular.module('app').factory('loggerService', loggerService);
    loggerService.$inject = [];

    function loggerService() {
        let self = this;
        self.log = log;

        return self;

        //---
        function log(message) {
            console.log(message);
        }
    }
})();