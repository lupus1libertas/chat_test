(() => {
    'use strict';
    angular.module('app').factory('userService', userService);
    userService.$inject = ['appConfig', '$http', '$rootScope', 'loggerService', '$sessionStorage'];

    function userService(appConfig, $http, $rootScope, loggerService, $sessionStorage) {
        let self = this;        
        initData();

        self.getUserName = getUserName;
        self.getToken = getToken;
        self.userLogined = userLogined;
        self.login = login;
        self.logout = logout;
        self.register = register;

        return self;

        //---

        // --- Public

        function getToken() {
          return self._token;
        }

        function register(username, password) {
            return $http({
                url: `${appConfig.api_root_url}/api/register/`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                data: {
                    username,
                    password
                }
            }).then(onUserManageSuccess)
            .catch((errorText) => {
                loggerService.log(`Error in userService:register Error text: ${errorText}`);
                throw new Error(errorText);
            });
        }

        function login(username, password) {
            return $http({
                url: `${appConfig.api_root_url}/api/login/`,
                method: "POST",
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                data: {
                    username,
                    password
                }
            }).then(onUserManageSuccess)
            .catch((errorData) => {
                loggerService.log(`Error in userService:login Error text: ${errorData.data.message}`);
                throw new Error(errorData.data.message)
            });
        }

        function logout() {
            clearSessionStorage();
            initData();
        }

        function getUserName() {
            return self._username;
        }

        function userLogined() {
            return self._userLoginedFlag;
        }

        // --- end Public


        // --- Utils

        function clearSessionStorage() {
          setSessionStorage(undefined, undefined);
        }

        function setSessionStorage(username, token) {
          $sessionStorage.username = username;
          $sessionStorage.token = token;
        }

        function initData() {
          self._username = $sessionStorage.username || undefined;
          self._userLoginedFlag = $sessionStorage.token ? true : false;
          self._token = $sessionStorage.token || undefined;
        }

        // --- end Utils

        // --- Handlers
        
        function onUserManageSuccess(response) {
            if (!response.data.success) {
                throw new Error(response.data.message);
            }
            setSessionStorage(response.config.data.username, response.data.token);
            initData();
                
            return response;
        }

        // --- end Handlers
    }
})();