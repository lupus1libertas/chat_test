(() => {
    'use strict';

    angular.module('app').constant('appConfig', new AppConfig());

    function AppConfig(){
        this.api_root_url = window.location.origin;
        this.serverDomain = window.location.host;
        
        // When we're using HTTPS, use WSS too.
        this.ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
        this.socketServerUrl = `${this.ws_scheme}://${this.serverDomain}`;
        // this.api_static = this.api_root_url + '/static/';
    }
})();