(() => {
    'use strict';
    
    angular.module('app').controller('UserManagementController', UserManagementController);
    UserManagementController.$inject = ['userService', '$uibModal', 'loggerService', '$rootScope'];

    function UserManagementController(userService, $uibModal, loggerService, $rootScope) {
        let self = this;
        initData();
        
        self.login = login;
        self.logout = logout;
        self.register = register;

        const formParams = {
            animation: true,
            controller: 'LoginFormController as form',
            size: 'md'
        };

        //---

        // --- Public

        function login() {
            const formParamsClone = Object.assign({
                templateUrl: "/static/app/user-management/templates/login-form.html",
            }, formParams);
            
            const loginInstance = $uibModal.open(formParamsClone);

            loginInstance.result
            .then((result) => {
                userService.login(result.login, result.password)
                .then(loginSuccess)
                .catch((errorText) => {
                    alert(errorText);
                });
            });
        }

        function logout() {
            userService.logout();
            self.userLogined = userService.userLogined();
            self.userName = userService.getUserName();
            $rootScope.$emit('UserManagementController:UserLoggedOut');
        }

        function register() {
            const formParamsClone = Object.assign({
                templateUrl: "/static/app/user-management/templates/register-form.html",
            }, formParams);
            
            const registerInstance = $uibModal.open(formParamsClone);

            registerInstance.result
            .then((result) => {
                userService.register(result.login, result.password)
                .then(registerSuceess)
                .catch((errorText) => {
                    alert(errorText);
                });
            });
        }
        
        // --- end Public

        // --- Utils

        function initData() {
            self.userLogined = userService.userLogined();
            self.userName = userService.getUserName();
        }
        
        // --- end Utils

        // --- Handlers

        function loginSuccess(response) {
            initData();
            $rootScope.$emit('UserManagementController:UserLogined');
        }

        function registerSuceess() {
            initData();
            $rootScope.$emit('UserManagementController:UserRegistered');
        }

        // --- end Handlers

    }
})();