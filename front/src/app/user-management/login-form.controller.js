(() => {
    'use strict';
    
    angular.module('app').controller('LoginFormController', LoginFormController);
    LoginFormController.$inject = ['$uibModalInstance'];

    function LoginFormController($uibModalInstance) {
        let self = this;

        self.login = '';
        self.password = '';

        self.submit = submit;
        self.cancel = cancel;
        
        //---
        function submit() {
            var result = {
                login: self.login,
                password: self.password
            }
            $uibModalInstance.close(result)
        }

        function cancel() {
            const reason = {
                msg: 'click on cancel'
            }
            $uibModalInstance.dismiss(reason)
        }
    }
})();